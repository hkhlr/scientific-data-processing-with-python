# SPDX-FileCopyrightText: © 2021 HPC Core Facility of the Justus-Liebig-University Giessen <philipp.e.risius@theo.physik.uni-giessen.de>,<marcel.giar@physik.jlug.de>
# SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <tim.jammer@hpc-hessen.de>, <marcel.giar@hpc-hessen.de>
#
# SPDX-License-Identifier: MIT

import urllib
from os import makedirs, path
from pathlib import Path

import pandas as pd


def download_IRIS(url="https://archive.ics.uci.edu/ml/machine-learning-databases/iris/"):
    datafile = 'iris.data'
    namesfile = 'iris.names' 
    
    output_path = Path('tmp')
    output_datafile = output_path / "iris-data.csv"
    
    makedirs(output_path, exist_ok=True)
    
    column_names = ["sepal length", "sepal width", 'petal length', 'petal width', "Name"]
    if not path.exists(output_datafile):
        print(f"Will be downloading Iris dataset...")
        with urllib.request.urlopen(url + datafile) as response, open(output_datafile, "w", encoding="utf-8") as out_file:
            data = response.read()
            out_file.write(",".join(column_names) + "\n")
            out_file.write(data.decode('utf-8'))
    else:
        print(f"No need to download Iris dataset. Data is already present in {output_datafile}.")
    
    df = pd.read_csv(output_datafile, delimiter=',')
    
    return df

def download_IRIS_with_addons(url="https://archive.ics.uci.edu/ml/machine-learning-databases/iris/",
                              delimiter=None, datafile = 'iris.data', namesfile = 'iris.names'):
    output_path = Path("tmp_with_addons")
    output_datafile = output_path / "iris-data.csv"
    makedirs(output_path, exist_ok=True)
    
    column_names = ["sepal length", "sepal width", 'petal length', 'petal width', "Name"]
    with urllib.request.urlopen(url + datafile) as response, open(output_datafile, "w", encoding="utf-8") as out_file:
        data = response.read()
        for cname in column_names[:-1]:
            out_file.write(f"# {cname} is in [cm]\n") # We use the '#' symbols for comments.
        out_file.write("# Species:\n# - Iris Setosa\n# - Iris Versicolour\n# - Iris Virginica\n")
        if delimiter is None:
            out_file.write(",".join(column_names) + "\n")
            out_file.write(data.decode('utf-8'))
        else:
            out_file.write(f"{delimiter}".join(column_names) + "\n")
            out_file.write(data.decode("utf-8").replace(",", delimiter))