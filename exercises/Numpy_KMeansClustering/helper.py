# SPDX-FileCopyrightText: © 2021 HPC Core Facility of the Justus-Liebig-University Giessen <philipp.e.risius@theo.physik.uni-giessen.de>,<marcel.giar@physik.jlug.de>
# SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <tim.jammer@hpc-hessen.de>, <marcel.giar@hpc-hessen.de>
#
# SPDX-License-Identifier: MIT

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def init_figure(figsize=(8, 8)):
    _, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=figsize)
    return ax1, ax2


# We have not dealt with `matplotlib` (or other packages for plotting data) yet
# but it is quite convenient for the purpose of visualising the results of the
# cluster search.
def make_scatter_plot(
    ax,
    coords,
    labels,
    markers=None,
    colors=None,
    with_legend=False,
    figname=None,
):
    ax.set_aspect("equal")
    ax.minorticks_on()

    if colors is None:
        cmap = plt.get_cmap("tab10")
        color_list = [cmap(idx) for idx in range(len(labels))]
    else:
        color_list = colors

    marker_list = (
        list(Line2D.filled_markers)[: len(labels)] if markers is None else markers
    )

    for xy, col, ll, mm in zip(coords, color_list, labels, marker_list):
        try:
            x, y = xy.transpose()
        except AttributeError:
            x, y = [c[0] for c in xy], [c[1] for c in xy]
        ax.scatter(x, y, s=20, color=col, label=ll, marker=mm)

    if with_legend:
        ax.legend(bbox_to_anchor=(1, 1), loc="upper left")

    if figname is not None:
        plt.savefig(figname, bbox_inches="tight")

def plot_clustering(n_clusters,coords,coords_center,center_labels):
    fig, ax = plt.subplots()

    # Assigen each point to a cluster.
    coords_labelled = list(
        coords[center_labels == tt] for tt in range(n_clusters)
    )
    # Plot clusters with colors according to which cluster they belong.
    make_scatter_plot(
        ax,
        coords_labelled, 
        labels=[f"cluster {tt}" for tt in range(n_clusters)],
        markers=["o"] * n_clusters
    )
    # Plot cluster centers.
    make_scatter_plot(
        ax,
        coords_center, 
        labels=[f"centeroid {tt}" for tt in range(n_clusters)],
        colors=["black"] * n_clusters,
        with_legend=True,
#         figname="kmeans.pdf"
    )
    plt.show()
  
        

def read_cluster_data(filename):
    """Helper function to read sample datasets."""
    with open(filename, "r", encoding="utf-8") as datafile:
        coords, labels = [], []
        for line in datafile:
            x, y, l = map(float, line.split(","))
            coords.append([x, y])
            labels.append(int(l))
    return coords, labels
