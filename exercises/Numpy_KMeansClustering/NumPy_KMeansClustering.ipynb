{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9ccf7386",
   "metadata": {},
   "source": [
    "# $K$-Means Clustering"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "random-contract",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "from matplotlib import pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "import importlib\n",
    "import helper\n",
    "importlib.reload(helper)\n",
    "\n",
    "from IPython.display import clear_output\n",
    "from time import sleep, time"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ae69504",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "## Introduction\n",
    "$K$-Means Clustering is a method from classical machine learning. It is used to find $K$ different groups of similar items in a dataset.\n",
    "\n",
    "In our case the dataset is a set of $N$ 2-dimensional coordinate vectors $\\vec{x}_1,\\vec{x}_2,\\dots,\\vec{x}_N$. These points form $K < N$ clusters which we would like to find. In order to characterise a cluster we use the cluster centre $\\vec{\\mu}_j$ ($1 \\leq j \\leq K$). *Each* point from the size-$N$ set can be assigned to *one* of these clusters (we will limit ourselves to cases where this indeed is possible)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8075aed",
   "metadata": {},
   "source": [
    "## Algorithm\n",
    "Assigning a point to a cluster works according to the following procedure:\n",
    "\n",
    "1. **Initialisation**: Randomly choose cluster centres $\\vec{\\mu}_j$ ($1 \\leq j \\leq K$). A simple way to achieve this is to choose them from the set of points $\\{\\vec{x}_i\\}_{i = 1, \\dots, N}$.\n",
    "\n",
    "2. **Iterations**: \n",
    "    - For all $i = 1, \\dots N$ find the cluster centre with position $\\vec{\\mu}_j$ to which $\\vec{x}_i$ has the *smallest* euclidian distance:\n",
    "    $$\n",
    "    c^{(i)} = \\operatorname{argmin}_{j \\in \\{1, \\dots, K\\}} \\left\\|\\vec{x}_i - \\vec{\\mu}_j\\right\\|_2^2,\n",
    "    $$\n",
    "    where $\\|\\vec{x}\\|_2 = \\sqrt{x_1^2 + x_2^2}$. $c^{(i)}$ is an integer number from the set $\\{1, \\dots, K\\}$. We use is to assign an index to each point $\\vec{x}_i$ (being $c^{(i)}$). This index designated the cluster centre to which the $i$th point is closest to. Hence, for each of the points we must compute the (squared) distance to *all* cluster centres $\\vec{\\mu}_j$ ($1 \\leq j \\leq K$) and determine the smallest of these distances. The index $j$ of the cluster with the smallest distance to a point with index $i$ is assigned to $c^{(i)}$.\n",
    "    - After having assigned each point of the set $\\{\\vec{x}_i\\}_{i = 1, \\dots, N}$ re-compute the position of all cluster centers:\n",
    "    $$\n",
    "    \\vec{\\mu}_j = \\frac{1}{n_j} \\sum_{\\vec{x}_i\\text{ with }c^{(i)} = j} \\vec{x}_i,\n",
    "    $$\n",
    "    By $n_j$ we mean the total number of points for which $c^{(i)} = j$. The *new* cluster centre is nothing but the arithmetic mean of all points $\\vec{x}_i$ that were assigned to the previous cluster centre.\n",
    "    - We compare the set cluster centres $C^{\\mathrm{old}} = \\{\\vec{\\mu}_1^{\\mathrm{old}}, \\dots, \\vec{\\mu}_K^{\\mathrm{old}} \\}$ from the previous iteration and the current set of cluster centres  $C = \\{\\vec{\\mu}_1, \\dots, \\vec{\\mu}_K \\}$. If cluster centres are pair-wise equal (compare those with the same index) we stop the iterations. We have reached a steady state and the algorithm has *converged*."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba899ad5",
   "metadata": {},
   "source": [
    "## Task formulation\n",
    "\n",
    "Implement the outlined algorithm for the method of $K$-Means Clustering. Stick to the paradigm of *array-oriented programming* as often as possible.\n",
    "\n",
    "In case you have trouble mapping the algorithm to Numpy commands and functions it can help to first implement it with standard Python only.\n",
    "\n",
    "The folder `sample-data` contains some sample-dataset that you can use to explore the algorithm and your implementation.\n",
    "\n",
    "*Hint*: It can be helpful to plot the data and the cluster centres determined with your implementation. Have a look at the `make_scatter_plot` function from the `helper.py` module provided with this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25abbe32",
   "metadata": {},
   "outputs": [],
   "source": [
    "n_clusters = 2\n",
    "dataset = np.loadtxt(f\"sample-data/coords-with-labels-{n_clusters}.dat\", delimiter=\",\")\n",
    "coords, labels = dataset.T[:2].T, dataset.T[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5bf0a0e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "helper.make_scatter_plot(\n",
    "        ax,\n",
    "        [coords[labels == tt] for tt in range(n_clusters)], \n",
    "        labels=[f\"cluster {tt}\" for tt in range(n_clusters)],\n",
    "        markers=[\"o\"] * n_clusters\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21546ed7",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "62ababfe",
   "metadata": {},
   "source": [
    "## Implementation of solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "european-bookmark",
   "metadata": {},
   "outputs": [],
   "source": [
    "# return True, if centers have not changed and the algorithm can therefore stop\n",
    "def centers_have_not_changed(a, b):\n",
    "    # Provide your implementation here.\n",
    "    return np.allclose(a,b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ahead-antenna",
   "metadata": {},
   "outputs": [],
   "source": [
    "# return the updated locations of the cluster centers\n",
    "def compute_centers(coords, labels, n_centers):\n",
    "    # Provide your implementation here. \n",
    "    # **HINT**:\n",
    "    # \n",
    "    # Use advanced indexing with boolean masks to access\n",
    "    # all points that have a label corresponding to the \n",
    "    # index of a cluster center.\n",
    "    return np.array([coords[labels == idx].mean(axis=0) for idx in range(n_centers)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "known-travel",
   "metadata": {},
   "outputs": [],
   "source": [
    "# return the list of *indices* of the cluster centers for the coordinates\n",
    "def find_closest_center(coords, coords_center):\n",
    "    # Provide your implementation here.\n",
    "    # **HINT**:\n",
    "    # \n",
    "    # Use `np.tile()` to augment `coords` and then make use\n",
    "    # of NumPy's implicit broadcasting capabilities to\n",
    "    # compute the distance of each point to *all* cluster\n",
    "    # centers. You might also need to reshape the array.\n",
    "    # Think about along which *axis* to compute the norm. \n",
    "    #\n",
    "    # Then select the *index* of cluster center with the \n",
    "    # least distance for each point (Look up the \n",
    "    # `np.argmin()` function.).\n",
    "    n_centers = coords_center.shape[0]\n",
    "    coords_shifted = np.reshape(\n",
    "        np.tile(coords, (1, n_centers)) - coords_center.ravel(),\n",
    "        (coords.shape[0], n_centers, coords.shape[1]),\n",
    "    )\n",
    "    return np.argmin(np.linalg.norm(coords_shifted, axis=2), axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "physical-saturday",
   "metadata": {},
   "outputs": [],
   "source": [
    "# The driver function - You need to change it, as ther is an error in it\n",
    "# the error is *not* in the visualization part\n",
    "def kmeans(coords, n_centers, n_iter, initial_random_state=42,visualize_progres=True,sleep_time=0.5):\n",
    "    # Initialise the coordinates of the cluster centers\n",
    "    rng = np.random.RandomState(initial_random_state)\n",
    "    index = rng.choice(coords.shape[0], n_centers, replace=False)\n",
    "    \n",
    "    # Store coords of the center for iterations\n",
    "    coords_center = coords[index, ...].copy()\n",
    "    coords_center_old = coords_center.copy()\n",
    "    \n",
    "    for i in range(n_iter):\n",
    "        # Find closest center for each point\n",
    "        ### --> you provide this function ###\n",
    "        labels = find_closest_center(coords, coords_center)\n",
    "        if visualize_progres:\n",
    "            # Visualization of the process\n",
    "            sleep(sleep_time) \n",
    "            clear_output(wait=True)\n",
    "            helper.plot_clustering(n_centers,coords,coords_center,labels)\n",
    "       \n",
    "        # Update the centeroids\n",
    "        # INFO: \"...\" in x[...] is a slicing operation called \"ellipsis\". You can learn\n",
    "        # more about it here: https://stackoverflow.com/questions/118370/how-do-you-use-the-ellipsis-slicing-syntax-in-python\n",
    "        coords_center_old = coords_center # save old version for testing convergence\n",
    "        ### --> you provide this solution ###\n",
    "        # erronenous:\n",
    "        #coords_center[...] = compute_centers(coords, labels, n_centers)\n",
    "        # correct\n",
    "        coords_center = compute_centers(coords, labels, n_centers)\n",
    "        # Test for convergence\n",
    "        ### --> you provide this solution ###\n",
    "        if centers_have_not_changed(coords_center, coords_center_old):\n",
    "            if visualize_progress:\n",
    "                # visualize final state\n",
    "                sleep(sleep_time)\n",
    "                clear_output(wait=True)\n",
    "                helper.plot_clustering(n_centers,coords,coords_center,labels)\n",
    "            print(\"Finished after %d iterations\"%i)\n",
    "            break\n",
    "\n",
    "            \n",
    "    return coords_center, labels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bronze-advocate",
   "metadata": {},
   "outputs": [],
   "source": [
    "def main(n_clusters, dataset, n_iter=1000):\n",
    "#     coords, labels = dataset.T[:2].T, dataset.T[-1].astype(int)\n",
    "    coords  = dataset.T[:2].T\n",
    "    \n",
    "    coords_center, center_labels = kmeans(\n",
    "        coords=coords,# the input data (coordinates of the points to be clustered)\n",
    "        n_centers=n_clusters,# number of clusters\n",
    "        n_iter=n_iter,# maximum number of iterations to perform, if algorithm does not converge before\n",
    "        #initial_random_state=int(time()),# initial random seed - use a fixed value, if you want to have the same initial state for every execution\n",
    "        # this is a good random seed to see the bug\n",
    "        initial_random_state=4321,\n",
    "        visualize_progress=True,#Turn Off, if you do not want to wait for the visualization\n",
    "        sleep_time=1 # the sleep time controls the speed of the visualization (lower means faster)\n",
    "        \n",
    "    )\n",
    "    \n",
    "    print(coords_center)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "middle-planner",
   "metadata": {},
   "outputs": [],
   "source": [
    "if __name__ == \"__main__\":\n",
    "    n_clusters = 4 # change this value to test different datasets\n",
    "    dataset = np.loadtxt(f\"sample-data/coords-with-labels-{n_clusters}.dat\", delimiter=\",\")\n",
    "    main(n_clusters, dataset)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "surprising-austria",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91f9bc21",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
