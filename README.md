<!--
SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <tim.jammer@hpc-hessen.de>, <marcel.giar@hpc-hessen.de>

SPDX-License-Identifier: CC0-1.0
-->

# Scientific Data Processing With Python

## About this course

This course is part of the annual [HiPerCH workshop](https://www.hkhlr.de/en/events/hiperch-14-2022-09-12) offered by the Competence Center for High Performance Computing in Hessen ([HKHLR](https://www.hkhlr.de/)). It aims at teaching participants the basics of using the Python libraries Numpy and Pandas for data processing. For a detailed course description please refer to the [course website](https://www.hkhlr.de/en/articles/hiperch-14-module-1).


## Getting the course material 

The course material can be obtained by cloning this repository. E.g. from within a terminal emulator of your choice execute:

```shell
git clone https://git.rwth-aachen.de/hkhlr/scientific-data-processing-with-python.git
```

Please note that you need to have Git installed. This can e.g. be done via `sudo apt-get install git` on Debian-based Linux distros or `brew install git` on macOS with [Homebrew](https://brew.sh/) installed. Git for Windows can be obtained [here](https://git-scm.com/downloads).


## Setting up the environment

### Installing Python 

In order to follow the course you need a recent version of Python 3 (e.g. Python 3.9.x or 3.10.x). You can either download a particular version of Python from [its website](https://www.python.org/downloads/), install it with a package manager (e.g. `apt` on Debian-based Linux distros or `brew` on macOS), or install the Anaconda distribution (this might be the easiest option).

For installing Anaconda, you have several options:

* The [Anaconda Python](https://www.anaconda.com/products/distribution#Downloads) distribution.
* The [Miniconda Python](https://docs.conda.io/en/latest/miniconda.html) distribution.

Please download and [install](https://docs.anaconda.com/anaconda/install/) the distribution for your particular architecture and plattform. 


### Python Anaconda / Miniconda environment

#### Command line (Linux and macOS)

Open a terminal emulator of your choice. Then type and execute the following commands to create a `conda` environment (the `conda` tool is common to both Anaconda and Miniconda):

```shell
$ cd /path/to/course/directory         # Make sure to navigate to the course directory first!
$ conda env create -f environment.yml  # The environment file is located at the root of the directory containing this repository.
```

Afterwards, activate it:

```shell
$ conda activate scipython
```

#### Anaconda Navigator (e.g. Windows)

Follow the instructions provided [here](https://docs.anaconda.com/anaconda/navigator/tutorials/manage-environments) and use our `environment.yml` file.

Navigate to the section ["Importing an environment"](https://docs.anaconda.com/anaconda/navigator/tutorials/manage-environments/#importing-an-environment), read and execute the instructions of the first paragraph. The `environment.yml` file is located in the directory in which you have stored the repository for this course.


### Python virtual environments

For creating a Python-native virtual environment open a terminal emulator and execute the following commands: 

```shell
$ cd /path/to/course/directory                   # Make sure to navigate to the course directory first!
$ python3 -m venv .venv                          # Creates a virtual environment
$ source .venv/bin/activate
$ pip3 install --upgrade pip
$ pip3 install -r requirements.txt               # This file is located at the root of the directory containing this repository.
$ jupyter contrib nbextension install --sys-prefix
$ jupyter-nbextension install rise --py --sys-prefix
$ jupyter-nbextension enable rise --py --sys-prefix
```

Please refer to [this link](https://docs.python.org/3/library/venv.html) for how to create a virtual environment on Windows.
